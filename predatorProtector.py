#!/usr/bin/python3.2

"""
Code by Sacha Delanoue for Athens course Emergence in Complex System.

Problem: Each player chooses, at random, one other player to be their
"predator" and another to be their "protector". Every player makes such a
choice, and once these choices are made all the players move within the room
in accordance with one simple rule: keep the protector in a direct line between
yourself and the predator. A stable state is quickly reached.


To study the emerging pattern, I started by place individual at random on the
map, and then move them to maximize the vectorial product, thus trying to
make the line.

Stability is obtained because the individuals get stuck at the border of the
map. It is a strange way to stabilize though.
However, a few individual can stop moving in the middle of the map, because
their predator and protector are stuck in the border.

To run the code :
 - if the file is runnable (chmod +x), type ./predatorProtector.py args
 - or do python3.2 predatorProtector.py args
 - if you don't have python3, python2 is fine (but see below)
ags = population_size map_size number_obtacles

The map is then displayed (' ': void, '.': obstacle, 'X': individual), you can
hit ENTER to see the next stage.
If you use python2, you must enter a number (i.e. 0 or 1), before hitting
ENTER.
"""

from random import randrange, choice
from sys import argv
from math import sqrt

class Map(list):
    """ Represents the map where the individuals are. """

    def __init__(self, dimension, obstacles):
        """ Create a square map according to the dimension, with obstacles. """
        list.__init__(self, ([None] * dimension for i in range(dimension)))
        for i in range(obstacles):
            while True:
                x = randrange(dimension)
                y = randrange(dimension)
                if self[x][y] is None:
                    self[x][y] = '.'
                    break
        self.n = dimension

    def __repr__(self):
        """ Diplay the map in ASCII. """
        return "\n".join([" ".join(
                [' ' if j is None else
                 j if type(j) is str else
                 'X' for j in i]) for i in self])

    def emptySurronding(self, x, y):
        """ Generate the coordinates of the empty cases arround (x, y). """
        for dx, dy in ((-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 0), (0, 1),
                (1, -1), (1, 0), (1, 1)):
            if 0 <= x + dx < self.n and 0 <= y + dy < self.n:
                if self[x + dx][y + dy] is None:
                    yield (x + dx, y + dy)

    def getEmptyPos(self):
        """ Return a random empty position. """
        while True:
            x = randrange(self.n)
            y = randrange(self.n)
            if self[x][y] is None:
                break
        return (x, y)
            

class Individual():
    """ Represents an individual. """

    def __init__(self, n, nMax, matrix):
        """ Create the individual from n out of nMax in the map matrix. """
        self.n = n
        self._x, self._y = matrix.getEmptyPos()
        matrix[self._x][self._y] = self
        self.matrix = matrix
        others = list(range(nMax))
        others.remove(n)
        self.predator = choice(others)
        others.remove(self.predator)
        self.protector = choice(others)

    def __repr__(self):
        """ Display the individual: gives its number and position. """
        return "Individual {} ({}, {})".format(self.n, self.x, self.y)
    
    def _getX(self):
        """ Warning: setting x updates the map, erasing the previous data. """
        return self._x
    def _setX(self, x):
        self.matrix[self._x][self._y] = None
        self.matrix[x][self._y] = self
        self._x = x
    x = property(_getX, _setX)

    def _getY(self):
        """ Warning: setting y updates the map, erasing the previous data. """
        return self._y
    def _setY(self, y):
        self.matrix[self._x][self._y] = None
        self.matrix[self._x][y] = self
        self._y = y
    y = property(_getY, _setY)

    def setPos(self, x, y):
        """
        Update the position of the individual, and refresh the map.
        Note: accessing x and y variable also work but we often don't want an
        intermediate position (i.e. updating x, THEN y).
        """
        self.matrix[self._x][self._y] = None
        self.matrix[x][y] = self
        self._x = x
        self._y = y


class Group(list):
    """ Represents a group of individual, and their collective behaviour. """

    def __init__(self, number, size, obstacles):
        """
        Create the group of number individual, give the size of the map and the
        number of obstacles. """
        matrix = Map(size, obstacles)
        list.__init__(self,
                [Individual(i, number, matrix) for i in range(number)])
        self.matrix = matrix
        self.number = number

    def move(self):
        """ Make everyone move. """
        choices = []
        for indiv in self:
            x, y = indiv.x, indiv.y
            predator = self[indiv.predator]
            protector = self[indiv.protector]
            vx = protector.x - predator.x
            vy = protector.y - predator.y
            scalar = lambda x, y: (vx*(x - protector.x) + vy*(y - protector.y)
                    ) / sqrt((x - protector.x) ** 2 + (y - protector.y) **2)
            xMax, yMax = x, y
            value = scalar(x, y)
            for x2, y2 in self.matrix.emptySurronding(x, y):
                if scalar(x2, y2) > value:
                    xMax, yMax = x2, y2
                    value = scalar(x2, y2)
            choices.append((xMax, yMax))
            self.matrix[xMax][yMax] = "taken"
        for indiv, (x, y) in zip(self, choices):
            indiv.setPos(x, y)

if __name__ == "__main__":
    try:
        n = int(argv[1])
        size = int(argv [2])
        obstacles = int(argv[3])
    except IndexError:
        print("Syntax: {} population_size map_size obstacles".format(argv[0]))
    except ValueError:
        print("Arguments must be numbers.")
    else:
        group = Group(n, size, obstacles)
        while True:
            print(group.matrix)
            group.move()
            input()
